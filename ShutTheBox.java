public class ShutTheBox{
	
	public static void main (String [] args){
		
		System.out.println("Welcome to the Game!");
		
		Board board = new Board();
		boolean gameOver = false;
		
		while (!(gameOver)){
			
			System.out.println("Player 1's Turn");
			System.out.println(board);
			
			if(board.playATurn()){
				System.out.println("Player 2 Wins!");
				break;
			}
			
			System.out.println("Player 2's Turn");
			System.out.println(board);
			
			if(board.playATurn()){
				System.out.println("Player 1 Wins!");
				break;
			}
		}
			
	}
}
	
	
	
	
	
	
