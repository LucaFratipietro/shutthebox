import java.util.Random;

public class Die{
	
	private int pips;
	private Random rd;
	
	public Die() {
		this.pips = 1;
		this.rd = new Random();
	}
	
	public int getPips(){
		return this.pips;
	}

	public void roll(){	
		this.pips = (this.rd.nextInt(6) + 1);
	}
	
	public String toString(){
		return "You Rolled a " + this.pips;
	}
}
	