public class Board{
	
	private Die DiceOne;
	private Die DiceTwo;
	private boolean[] closedTiles;
	
	public Board(){
		
		this.DiceOne = new Die();
		this.DiceTwo = new Die();
		this.closedTiles = new boolean[12];
	
	}

	public String toString(){
		
		String message = "";
		
		for(int i = 0; i < this.closedTiles.length; i++){
			if (!(this.closedTiles[i])){
				message = message + " " + (i+1);
			}
		}
		return message;
	}
	
	public boolean playATurn(){
		
		this.DiceOne.roll();
		this.DiceTwo.roll();
		System.out.println(this.DiceOne.getPips());
		System.out.println(this.DiceTwo.getPips());
		
		int pipSum = this.DiceOne.getPips() + this.DiceTwo.getPips();
		
		if(!(this.closedTiles[(pipSum - 1)])){
			this.closedTiles[(pipSum - 1)] = true;
			System.out.println("Closing Tile: " + (pipSum));
			return false;
		}
		
		else{
			
			System.out.println("This tile is already shut");
			return true;
		}
		
	}
		
}
